package com.api;

import java.util.ArrayList;
import java.util.List;

import com.api.domain.Department;
import com.api.domain.Employee;
import com.api.view.DepartmentView;
import com.api.view.EmployeeView;

public class BaseTest {
	
	public static final int ID_1 = 1;

	public Employee getEmployee() {
		return Employee.of(ID_1, "employee1", "07-07-1990", "67 fairylane, Dearborn United States", getDepartments());
	}
	
	public List<Department> getDepartments() {
		List<Department> departments = new ArrayList<Department>();
		departments.add(getDepartment());
		return departments;
	}

	public Department getDepartment() {
		return Department.of(ID_1, "dept1");
	}
	
	public EmployeeView getEmployeeView() {
		EmployeeView view = new EmployeeView();
		view.setId(ID_1);
		view.setName("employee1");
		view.setDob("07-07-1990");
		view.setAddress("67 fairylane, Dearborn United States");
		view.setDepartments(getDepartmentViews());
		return view;
	}
	
	public List<DepartmentView> getDepartmentViews() {
		List<DepartmentView> departments = new ArrayList<DepartmentView>();
		DepartmentView department = new DepartmentView();
		department.setId(ID_1);
		department.setName("dept1");
		departments.add(department);
		return departments;
	}
}
