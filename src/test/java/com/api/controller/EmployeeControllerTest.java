package com.api.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.api.BaseTest;
import com.api.domain.Employee;
import com.api.service.EmployeeService;
import com.api.view.EmployeeView;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class EmployeeControllerTest extends BaseTest {

	@InjectMocks
	private EmployeeController controller;
	
	@Mock
	private EmployeeService service;
	
	@Mock
	private EmployeeView view;

	@Test
	public void insertEmployeeRecordWhenCreateEmployeeIsCalled() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(view.toEmployee(Mockito.any(EmployeeView.class))).thenReturn(employee);
		Mockito.when(service.create(employee)).thenReturn(employee);
		Mockito.when(view.toView(employee)).thenReturn(getEmployeeView());
		EmployeeView actual = controller.createEmployee(getEmployeeView());
		Assertions.assertEquals(ID_1, actual.getId());
		Assertions.assertEquals(employee.getName(), actual.getName());
		Assertions.assertEquals(employee.getDateOfBirth(), actual.getDob());
		Assertions.assertEquals(employee.getAddress(), actual.getAddress());
		Assertions.assertEquals(ID_1, actual.getDepartments().size());
		Assertions.assertEquals(ID_1, actual.getDepartments().get(0).getId());
		Assertions.assertEquals(employee.getDepartments().get(0).getName(), actual.getDepartments().get(0).getName());
	}
	
	@Test
	public void retrieveEmployeeRecordWhenFetchEmployeeById() throws Exception {
		EmployeeView employee = getEmployeeView();
		Mockito.when(service.fetchById(ID_1)).thenReturn(getEmployee());
		Mockito.when(view.toView(Mockito.any(Employee.class))).thenReturn(getEmployeeView());
		
		EmployeeView actual = controller.fetchById(ID_1);
		Assertions.assertEquals(ID_1, actual.getId());
		Assertions.assertEquals(employee.getName(), actual.getName());
		Assertions.assertEquals(employee.getDob(), actual.getDob());
		Assertions.assertEquals(employee.getAddress(), actual.getAddress());
		Assertions.assertEquals(ID_1, actual.getDepartments().size());
		Assertions.assertEquals(ID_1, actual.getDepartments().get(0).getId());
		Assertions.assertEquals(employee.getDepartments().get(0).getName(), actual.getDepartments().get(0).getName());
	}
}
