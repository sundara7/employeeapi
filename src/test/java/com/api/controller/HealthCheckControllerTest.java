package com.api.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class HealthCheckControllerTest {

	@InjectMocks
	private HealthCheckController controller;

	@Test
	public void returnSuccessWhenHealthCheckIsInvoked() throws Exception {
		String actual = controller.healthCheck();
		Assertions.assertEquals("Success", actual);
	}
}
