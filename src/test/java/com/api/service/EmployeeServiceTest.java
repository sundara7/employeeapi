package com.api.service;


import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.api.BaseTest;
import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.exception.ValidationException;
import com.api.repository.DepartmentRepository;
import com.api.repository.EmployeeRepository;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class EmployeeServiceTest extends BaseTest {

	@InjectMocks
	private EmployeeService service;

	@Mock
	private EmployeeRepository repository;

	@Mock
	private DepartmentRepository departmentRepository;
	
	@Captor
	private ArgumentCaptor<Employee> captor;
	
	@Mock
	private ValidationService validation;

	@Test
	public void insertEmployeeRecordWithoutDepartmentWhenCreate() throws Exception {
		Mockito.when(departmentRepository.findById(1)).thenReturn(
				Optional.of(getDepartment()));
		service.create(getEmployee());
		Mockito.verify(repository, Mockito.atLeastOnce()).save(captor.capture());
		Employee employee = captor.getValue();
		Assertions.assertEquals(1, employee.getDepartments().size());
	}
	
	@Test
	public void insertEmployeeRecordWithDepartmentToTableWhenCreate() throws Exception {
		Mockito.when(departmentRepository.findById(1)).thenReturn(
				Optional.ofNullable(null));
		service.create(getEmployee());
		Mockito.verify(repository, Mockito.atLeastOnce()).save(captor.capture());
		Employee employee = captor.getValue();
		Assertions.assertEquals(1, employee.getDepartments().size());
		Assertions.assertEquals(1, employee.getDepartments().get(0).getId());
	}
	
	@Test
	public void throwsValidationExceptionIncaseOfInvalidEmployeeObject() throws Exception {
		Mockito.doThrow(ValidationException.class).when(validation).validate(Mockito.any(Employee.class));
		
		Throwable throwable =  Assertions.assertThrows(Throwable.class, () -> {
		    service.create(new Employee());
		});
		Assertions.assertEquals(ValidationException.class, throwable.getClass());
	}

	@Test
	public void retrieveEmployeeRecordFromTableWhenFetchById() throws Exception {
		Employee employee = getEmployee();
		Mockito.when(repository.findById(ID_1)).thenReturn(
				Optional.of(employee));
		Employee actual = service.fetchById(ID_1);
		Assertions.assertEquals(ID_1, actual.getId());
		Assertions.assertEquals(employee.getName(), actual.getName());
		Assertions.assertEquals(employee.getDateOfBirth(),
				actual.getDateOfBirth());
		Assertions.assertEquals(employee.getAddress(), actual.getAddress());
		Assertions.assertEquals(ID_1, actual.getDepartments().size());
		Assertions.assertEquals(ID_1, actual.getDepartments().get(0).getId());
		Assertions.assertEquals(employee.getDepartments().get(0).getName(),
				actual.getDepartments().get(0).getName());
	}

	@Test
	public void throwsRecordNotFoundExceptionWhenEmployeeNotFoundById()
			throws Exception {
		RecordNotFoundException exception = Assertions.assertThrows(
				RecordNotFoundException.class, () -> {
					service.fetchById(ID_1);
				});
		Assertions.assertEquals("Employee record not found",
				exception.getMessage());
	}
}
