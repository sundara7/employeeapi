package com.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.api.BaseTest;
import com.api.domain.Employee;
import com.api.exception.ValidationException;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest extends BaseTest {

	@InjectMocks
	private ValidationService service;

	@Test
	public void doesNotThrowExceptionWhenAllRequiredFieldsPresent()
			throws Exception {
		service.validate(getEmployee());
	}
	
	@Test(expected = ValidationException.class)
	public void throwValidationExceptionWhenEmployeeIdIsEmpty()
			throws Exception {
		Employee employee = getEmployee();
		employee.setId(null);
		service.validate(employee);
	}
	
	@Test(expected = ValidationException.class)
	public void throwValidationExceptionWhenEmployeeNameIsEmpty()
			throws Exception {
		Employee employee = getEmployee();
		employee.setName(null);
		service.validate(employee);
	}
	
	@Test(expected = ValidationException.class)
	public void throwValidationExceptionWhenBothEmployeeIdAndNameAreEmpty()
			throws Exception {
		service.validate(new Employee());
	}
}
