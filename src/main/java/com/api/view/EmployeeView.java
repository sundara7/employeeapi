package com.api.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.domain.Employee;

@Service
public class EmployeeView {
	
	@Autowired
	private DepartmentView departmentView;

	private Integer id;
	private String name;
	private String dob;
	private String address;
	private List<DepartmentView> departments;
	
	public Employee toEmployee(EmployeeView view) {
		Employee employee = new Employee();
		employee.setId(view.getId());
		employee.setName(view.getName());
		employee.setDateOfBirth(view.getDob());
		employee.setAddress(view.getAddress());
		employee.setDepartments(departmentView.toDepartments(view.getDepartments()));
		return employee;
	}
	
	public EmployeeView toView(Employee employee) {
		EmployeeView view = new EmployeeView();
		view.setId(employee.getId());
		view.setName(employee.getName());
		view.setDob(employee.getDateOfBirth());
		view.setAddress(employee.getAddress());
		view.setDepartments(departmentView.toViews(employee.getDepartments()));
		return view;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<DepartmentView> getDepartments() {
		return departments;
	}

	public void setDepartments(List<DepartmentView> departments) {
		this.departments = departments;
	}
}
