package com.api.view;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.api.domain.Department;

@Service
public class DepartmentView {
	private Integer id;
	private String name;
	
	public List<Department> toDepartments(List<DepartmentView> views) {
		List<Department> departments = new ArrayList<Department>();
		for (DepartmentView view : views) {
			departments.add(toDepartment(view));
		}
		return departments;
	}
	
	public List<DepartmentView> toViews(List<Department> departments) {
		List<DepartmentView> views = new ArrayList<DepartmentView>();
		for (Department department : departments) {
			views.add(toView(department));
		}
		return views;
	}
	
	private DepartmentView toView(Department department) {
		DepartmentView view = new DepartmentView();
		view.setId(department.getId());
		view.setName(department.getName());
		return view;
	}

	private Department toDepartment(DepartmentView view) {
		Department department = new Department();
		if (view.getId() != null) {
			department.setId(view.getId());
		}
		department.setName(view.getName());
		return department;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
