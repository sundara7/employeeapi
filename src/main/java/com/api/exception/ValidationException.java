package com.api.exception;

public class ValidationException extends Exception {

	private static final long serialVersionUID = 435862277665465138L;
	
	public ValidationException(String message) {
		super(message);
	}

}
