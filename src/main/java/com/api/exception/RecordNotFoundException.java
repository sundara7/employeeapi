package com.api.exception;

public class RecordNotFoundException extends Exception {

	private static final long serialVersionUID = -8641314942234073966L;

	public RecordNotFoundException(String message) {
		super(message);
	}

}
