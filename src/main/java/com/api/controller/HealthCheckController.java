package com.api.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HealthCheckController {

	@GetMapping(value="/healthcheck", produces = MediaType.APPLICATION_JSON_VALUE)
	public String healthCheck() {
		return "Success";
	}

}
