package com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.exception.ValidationException;
import com.api.service.EmployeeService;
import com.api.view.EmployeeView;

@RestController
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	private EmployeeService service;
	
	@Autowired
	private EmployeeView view;

	@PostMapping(value = "/employee", consumes = MediaType.APPLICATION_JSON_VALUE)
	public EmployeeView createEmployee(@RequestBody EmployeeView employeeView) throws ValidationException {
		Employee employee = service.create(view.toEmployee(employeeView));
		return view.toView(employee);
	}

	@GetMapping(value = "/employee/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public EmployeeView fetchById(@PathVariable Integer id) throws RecordNotFoundException {
		Employee employee = service.fetchById(id);
		return view.toView(employee);
	}

}
