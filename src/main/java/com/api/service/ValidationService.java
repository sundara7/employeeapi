package com.api.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.api.domain.Employee;
import com.api.exception.ValidationException;

@Service
public class ValidationService {

	public void validate(Employee employee) throws ValidationException {
		List<String> errorMessages = new ArrayList<String>();
		if (employee.getId() == null) {
			errorMessages.add("Employee Id cannot be null or empty");
		}
		if (StringUtils.isBlank(employee.getName())) {
			errorMessages.add("Employee Name cannot be null or empty");
		}
		if (!errorMessages.isEmpty()) {
			throw new ValidationException(StringUtils.join(errorMessages, "."));
		}
	}

}
