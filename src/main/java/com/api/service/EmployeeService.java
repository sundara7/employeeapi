package com.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.domain.Department;
import com.api.domain.Employee;
import com.api.exception.RecordNotFoundException;
import com.api.exception.ValidationException;
import com.api.repository.DepartmentRepository;
import com.api.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private ValidationService validation;

	public Employee create(Employee employee) throws ValidationException {
		validation.validate(employee);
		employee.setDepartments(getDepartments(employee));
		return repository.save(employee);
	}

	private List<Department> getDepartments(Employee employee) {
		List<Department> departments = new ArrayList<Department>();
		for (Department department : employee.getDepartments()) {
			Optional<Department> optional = departmentRepository.findById(department.getId());
			if (optional.isPresent()) {
				populateEmployee(employee, departments, optional.get());
			} else {
				populateEmployee(employee, departments, department);
			}
		}
		return departments;
	}

	private void populateEmployee(Employee employee,
			List<Department> departments, Department dept) {
		dept.setEmployee(employee);
		departments.add(dept);
	}

	public Employee fetchById(int id) throws RecordNotFoundException {
		if (repository.findById(id).isPresent()) {
			return repository.findById(id).get();
		}
		throw new RecordNotFoundException("Employee record not found");
	}

}
